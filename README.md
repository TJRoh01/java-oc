# NOTE: Project is W.I.P.
# Link between Java and OpenComputers mod for Minecraft

## Usage

### Minecraft
1. Enable HTTP
2. If you plan on running the Java server on your local machine you must remove 127.0.0.1 from the blacklist in OpenComputers config
3. Place a computer with an internet card (besides the other basic components)
4. Boot the computer with "EEPROM (Lua BIOS)"
5. Run "pastebin run 93xiti1B" and follow the instructions
6. Reboot in-game computer

## Global environment table
- _X = global namespace, use this for keeping APIs and variables loaded in memory between communications

## "Protected" communication variables (use but do not overwrite)
- _C = TCP socket connected to server
- _RX = server -> client message receiver, blocks until a message is avaible and returns a String, can not receive nil and empty String
- _TX = client -> server message sender, can not send nil and empty String

## Reserved _C TCP socket messages, do not send theese on your own, they will be read by the server as statuses, come up with your own names
- _NONE (empty message since you can not send nil or empty String)
- _TRUE (true)
- _FALSE (false)
- _UNCOM (received code is uncompileable)
- _READY (client is ready to receive next message)
