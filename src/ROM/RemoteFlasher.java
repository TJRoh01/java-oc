/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ROM;

import Tools.FS;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author tomas
 */
public class RemoteFlasher {

    private final String flasher;
    private final String rom;
    private final String checksum;

    public RemoteFlasher() throws FileNotFoundException, IOException, Exception {
        this.rom = FS.read("src/ROM/PXE_ROM.lua", 4096);
        if (System.getProperty("line.separator").equals("\n")) {
            this.checksum = FS.read("src/ROM/PXE_ROM_UNIX.CHECKSUM", 8);
        } else if (System.getProperty("line.separator").equals("\r\n")) {
            this.checksum = FS.read("src/ROM/PXE_ROM_DOS.CHECKSUM", 8);
        } else {
            throw new Exception("Unknown storage format");
        }
        this.flasher = FS.read("src/ROM/REMOTE_FLASHER.lua", 4096);
    }

    public String toString() {
        // [===[ text ]===] is a multiline String in Lua
        String prod = "local rom = [===[" + rom + "]===]\nlocal checksum = '" + checksum + "'\n" + flasher;
        return prod;
    }

}
