-- get EEPROM data
local eeprom = component.list("eeprom")()
local inet = string.gmatch((component.invoke(eeprom, "getData")), "([^:]+)") -- split by :
local ip = inet()
local port = tonumber(inet())

-- get Internet Card
local internet = component.list("internet")()

-- verify hardware and EEPROM data
if not (internet) then
    error("No Internet Card found", 0)
elseif not (component.invoke(internet, "isTcpEnabled")) then
    error("TCP is not enabled in mod config")
elseif not (type(ip) == "string") then
    error("Invalid IP")
elseif not (type(port) == "number") then
    error("Invalid PORT")
end

-- try to connect, computer will reboot if connection breaks/does not connect
_C = component.invoke(internet, "connect", ip, port)

-- send
function _TX(msg)
    _C.write(tostring(msg))
end

-- advanced rx (blocking)
function _RX()
    
    -- simple receiver
    local function rx()
        -- this will crash when the connection fails/closes
        local msg = _C.read(math.huge)
            -- return nil if "", easier to handle
            if string.len(msg) > 0 then
                return msg
            end
    end
    
    -- actual receiver
    local msg = nil
    repeat
        local buffer = ""
        repeat
            local data = nil
            local status, res = pcall(rx)

            -- rx fails if there is no connection, restart PXE boot process
            if (status) then
                data = res
            else
                computer.shutdown(true)
            end
            
            buffer = buffer .. (data or "")
        until not data
            if (string.len(buffer) > 0) then
                msg = buffer
            end
    until msg
    
    -- inform server client is ready for new messages
    _TX("_READY")
    return msg
end

-- ensure there is a connection
local est = nil
repeat
    est = _RX()
until est == "est"

-- send checksum
_TX(component.invoke(eeprom, "getChecksum"))

-- hand over execution to remote environment
f = load(_RX(), "=PXE_REMOTE")

if not (f) then
    _TX("_UNCOM")
    _TX("=PXE_REMOTE")
else       
    -- setup global space
    _X = {}
    
    -- dump variables/functions that will no longer be useful
    eeprom = nil
    inet = nil
    ip = nil
    port = nil
    internet = nil
    est = nil
    
    -- hand over execution in protected mode so that eventual error can be sent to server
    local status, res = pcall(f)
    
    if not (res) then
        res = "_NONE"
    end
    
    if (status) then
        _TX("_TRUE")
        _TX(res)
    else
        _TX("_FALSE")
        _TX(res)
    end
end

-- reboot computer if execution would end or code is uncompileabe
computer.shutdown(true)