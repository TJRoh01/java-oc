-- get EEPROM
local eeprom = component.list("eeprom")()

-- write to EEPROM
component.invoke(eeprom, "set", rom)
component.invoke(eeprom, "setLabel", "Java-OC PXE")


-- verify written data
if not (component.invoke(eeprom, "getChecksum") == checksum) then
    error("Failure writing EEPROM, unexpected checksum: " .. component.invoke(eeprom, "getChecksum"))
elseif not (component.invoke(eeprom, "get") == rom) then
    error("Failure writing EEPROM, content mismatch")
elseif not (component.invoke(eeprom, "getLabel") == "Java-OC PXE") then
    error("Failure writing EEPROM, label mismatch")
end

return "_TRUE"

-- will reboot on its own through PXE_ROM
