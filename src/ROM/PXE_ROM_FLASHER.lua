local rom_link = "https://pastebin.com/raw/0Lrr4aTw"
local checksum_link = "https://pastebin.com/raw/KuP6mJGf"

local component = require("component")
local internet = component.internet

-- check hardware and config
if not (internet) then
    error("No Internet Card found")
elseif not (internet.isTcpEnabled()) then
    error("TCP is not enabled in mod config")
elseif not (internet.isHttpEnabled()) then
    error("HTTP is not enabled in mod config")
end

local function dl(address)
    
    local remote = internet.request(address)
    local buffer = ""

    if not (remote.finishConnect()) then
        repeat
            local data = remote.read(math.max)
            if (type(data) == "string") then
                buffer = buffer .. data
            end
        until not data
    else
        error("Could not connect to remote resource")
    end

    remote.close()
    return buffer

end

-- download
io.write("Downloading resources\n")
io.write("> Downloading PXE_ROM.lua\n")
rom = dl(rom_link)
io.write("> Downloading PXE_ROM.CHECKSUM\n")
checksum = dl(checksum_link)
io.write("DONE\n\n")

-- get config
io.write("NOTE: if you intend to connect to 127.0.0.1 (localhost) you must first remove it from the blacklist in OpenComputers config file\n\n")
io.write("Input IP address of Java-OC Server:\n> ")
local ip
repeat
    ip = io.read()
until ip

io.write("Input PORT of Java-OC Server:\n> ")
local port
repeat
    port = io.read()
until port

-- write to EEPROM
io.write("\nInsert EEPROM to be written\n")
io.write("When ready to write, type `y` to confirm\n> ")
repeat
    local response = io.read()
until response and response:lower():sub(1, 1) == "y"

io.write("\nWriting to EEPROM... ")
local eeprom = component.eeprom
eeprom.setData(ip .. ":" .. port)
eeprom.set(rom)
eeprom.setLabel("Java-OC PXE")
io.write("DONE\n")

-- verify written data
io.write("\nVerifying EEPROM... ")
if not (eeprom.getData() == ip .. ":" .. port) then
    error("Failure writing EEPROM, config mismatch")
elseif not (eeprom.getChecksum() == checksum) then
    error("Failure writing EEPROM, checksum mismatch\nUnexpected checksum: " .. eeprom.getChecksum())
elseif not (eeprom.get() == rom) then
    error("Failure writing EEPROM, content mismatch")
elseif not (eeprom.getLabel() == "Java-OC PXE") then
    error("Failure writing EEPROM, label mismatch")
end
io.write("SUCCESS!\n")
