/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OpenComputers.Objects;

import Exceptions.ClientError;
import Exceptions.DoubleKeyNotSupported;
import Exceptions.NoReadyStatus;
import Exceptions.UncompileableCode;
import Lua.Table;
import OpenComputers.Objects.Components.Computer;
import OpenComputers.Objects.Components.EEPROM;
import OpenComputers.Objects.Components.Redstone;
import TCP.Connection;
import Tools.FS;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author tomas
 */
public class Generic {
    
    private final ExecutionLoop ix;
    public Table components;
    public Computer computer;
    public EEPROM eeprom;    
    public ArrayList<Redstone> redstone = new ArrayList<>();

    public Generic(Connection con) throws FileNotFoundException, IOException, InterruptedException, UncompileableCode, NoReadyStatus, ClientError, Exception {
        // establish execution loop
        this.ix = new ExecutionLoop(con);
        
        // send serialization
        this.ix.execute(FS.read("src/Snippets/serialization.lua", 8192));
        
        // wrap components
        wrap();
        // determine type
    }
    
    public void wrap() throws InterruptedException, UncompileableCode, ClientError, NoReadyStatus, DoubleKeyNotSupported, Exception {
        // purge current components
        this.computer = null;
        this.eeprom = null;
        this.redstone.clear();
        
        // get components
        String res = this.ix.execute("return serialization.serialize(component.list())");
        this.components = new Table(res);
        
        // wrap components
        for (String address : this.components.getStringKeys()) {            
            switch (components.getString(address)) {
                case "computer":
                    this.computer = new Computer(this.ix, address);
                    break;
                case "eeprom":
                    this.eeprom = new EEPROM(this.ix, address);
                    break;
                case "redstone":
                    this.redstone.add(new Redstone(this.ix, address));
                    break;
                case "gpu":
                    break;
                case "filesystem":
                    break;
                case "screen":
                    break;
                case "keyboard":
                    break;
                case "internet":
                    break;
                case "disk_drive":
                    break;
                default:
                    System.out.println("Uncaught component: " + components.getString(address));
            }
            
        }
    }

}
