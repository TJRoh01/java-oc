/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OpenComputers.Objects;

import Exceptions.ClientError;
import Exceptions.NoReadyStatus;
import Exceptions.UncompileableCode;
import TCP.Connection;
import TCP.Connection.Msg;
import Tools.FS;
import java.io.IOException;

/**
 *
 * @author tomas
 */
public class ExecutionLoop {
    
    private final Connection con;

    ExecutionLoop(Connection con) throws IOException, InterruptedException, UncompileableCode, NoReadyStatus, ClientError {
        this.con = con;
        con.send(FS.read("src/Snippets/EXECUTION_LOOP.lua", 4096), 1000);

        Msg res = null;
        try {
            res = con.recv(1000);
        } catch (ClientError ex) {
        }

        if (res != null) {
            if (res.status.equals("_UNCOM") && res.res.equals("=EXECUTION_LOOP")) {
                throw new UncompileableCode("Execution loop is not compileable");
            }
        }
    }

    public String execute(String ix, int timeout) throws InterruptedException, UncompileableCode, ClientError, NoReadyStatus {
        con.send(ix, timeout);
        Msg msg = con.recv(timeout);
        
        // error generating
        if (msg.status.equals("_UNCOM")) {
            throw new UncompileableCode("");
        } else if (msg.status.equals("_FALSE")) {
            throw new ClientError(msg.res);
        }

        return msg.res;
    }
    
    public String execute(String ix) throws InterruptedException, UncompileableCode, ClientError, NoReadyStatus {
        return execute(ix, 1000);
    }

}
