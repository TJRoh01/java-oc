/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OpenComputers.Objects.Components;

import OpenComputers.Objects.ExecutionLoop;
import Lua.Table;

/**
 *
 * @author tomas
 */
public class Redstone {
    
    private final ExecutionLoop ix;
    private final String address;

    public Redstone(ExecutionLoop ix, String address) throws InterruptedException, Exception {
        this.ix = ix;
        this.address = address;
        
        ix.execute("_X.redstone = component.proxy('" + address + "')");
    }
    
    public String address() {
        return address;
    }
    
    public double getInput(int side) throws InterruptedException, Exception {
        return Double.valueOf(ix.execute("return _X.redstone.getInput(" + side + ")"));
    }
    
    public Table getInput() throws InterruptedException, Exception {
        return new Table(ix.execute("return serialization.serialize(_X.redstone.getInput())"));
    }
    
    public double getOutput(int side) throws InterruptedException, Exception {
        return Double.valueOf(ix.execute("return _X.redstone.getOutput(" + side + ")"));
    }
    
    public Table getOutput() throws InterruptedException, Exception {
        return new Table(ix.execute("return serialization.serialize(_X.redstone.getOutput())"));
    }
    
    public double setOutput(int side, int strength) throws InterruptedException, Exception {
        return Double.valueOf(ix.execute("return _X.redstone.setOutput(" + side + "," + strength + ")"));
    }
    
    public Table setOutput(Table strengths) throws InterruptedException, Exception {
        return new Table(ix.execute("return serialization.serialize(_X.redstone.setOutput(" + strengths + "))")); //serialization.unserialize('')
    }
    
}
