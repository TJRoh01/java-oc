/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OpenComputers.Objects.Components;

import java.util.ArrayList;
import Lua.Table;
import OpenComputers.Objects.ExecutionLoop;


/**
 *
 * @author tomas
 */
public final class Computer {

    private final ExecutionLoop ix;
    private final String address;

    public Computer(ExecutionLoop ix, String address) throws InterruptedException, Exception {
        this.ix = ix;
        this.address = address;
        
        // set architecture to Lua 5.3
        if (!getArchitecture().equals("Lua 5.3")) {
            System.out.println("Switching CPU arch, must manually reboot later");
            setArchitecture("Lua 5.3");
        }

        // clear cache
        //ix.clear();
    }

    // computer address
    public String address() {
        return address;
    }

    // tmpfs address
    public String tmpAddress() throws InterruptedException, Exception {
        return ix.execute("return computer.tmpAddress()");
    }

    // control
    public void shutdown(boolean reboot) throws InterruptedException, Exception {
        if (!reboot) {
            ix.execute("computer.shutdown()");
        } else {
            reboot();
        }
    }

    public void reboot() throws InterruptedException, Exception {
        ix.execute("computer.shutdown(true)");
    }

    public double uptime() throws InterruptedException, Exception {
        return Double.parseDouble(ix.execute("return computer.uptime()"));
    }

    // beep
    public void beep(String pattern) throws InterruptedException, Exception { // pattern as . and -
        ix.execute("computer.beep('" + pattern + "')");
    }
    
    public void beep(int frequency) throws InterruptedException, Exception {
        ix.execute("computer.beep(" + frequency + ")");
    }

    public void beep(int frequency, int duration) throws InterruptedException, Exception {
        ix.execute("computer.beep(" + frequency + ", " + duration + ")");
    }

    // energy
    public double energy() throws InterruptedException, Exception {
        return Double.parseDouble(ix.execute("return computer.energy()"));
    }

    public double maxEnergy() throws InterruptedException, Exception {
        return Double.parseDouble(ix.execute("return computer.maxEnergy()"));
    }

    // memory
    public int freeMemory() throws InterruptedException, Exception {
        return Integer.valueOf(ix.execute("return computer.freeMemory()"));
    }

    public int totalMemory() throws InterruptedException, Exception {
        return Integer.valueOf(ix.execute("return computer.totalMemory()"));
    }

    // architecture
    public ArrayList<String> getArchitectures() throws InterruptedException, Exception {
        return (new Table(ix.execute("return serialization.serialize(computer.getArchitectures())"))).getStringValues();
    }

    public String getArchitecture() throws InterruptedException, Exception {
        return ix.execute("return computer.getArchitecture()");
    }

    public void setArchitecture(String architecture) throws InterruptedException, Exception {
        ix.execute("computer.setArchitecture('" + architecture + "')");
    }

}
