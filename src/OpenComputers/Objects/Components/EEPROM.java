/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OpenComputers.Objects.Components;

import OpenComputers.Objects.ExecutionLoop;

/**
 *
 * @author tomas
 */
public class EEPROM {

    private final ExecutionLoop ix;
    private final String address;

    public EEPROM(ExecutionLoop ix, String address) throws InterruptedException, Exception {
        this.ix = ix;
        this.address = address;
        
        ix.execute("_X.eeprom = component.proxy('" + address + "')");
    }
    
    public String address() {
        return address;
    }
    
    public String get() throws InterruptedException, Exception {
        return ix.execute("return _X.eeprom.get()");
    }
    
    public void set(String data) throws InterruptedException, Exception {
        ix.execute("_X.eeprom.set('" + data + "')");
    }
    
    public String getLabel() throws InterruptedException, Exception {
        return ix.execute("_X.eeprom.getLabel()");
    }
    
    public void setLabel(String label) throws InterruptedException, Exception {
        ix.execute("_X.eeprom.setLabel('" + label + "')");
    }
    
    public String getSize() throws InterruptedException, Exception {
        return ix.execute("_X.eeprom.getSize()");
    }
    
    public String getDataSize() throws InterruptedException, Exception {
        return ix.execute("_X.eeprom.getDataSize()");
    }
    
    public String getData() throws InterruptedException, Exception {
        return ix.execute("_X.eeprom.getData()");
    }
    
    public void setData(String data) throws InterruptedException, Exception {
        ix.execute("_X.eeprom.setData('" + data + "')");
    }
    
    public String getChecksum() throws InterruptedException, Exception {
        return ix.execute("_X.eeprom.getChecksum()");
    }
    
    public boolean makeReadonly() throws InterruptedException, Exception {
        String res = ix.execute("_X.eeprom.makeReadonly()");
        boolean out = false;
        
        if (res.equals("_TRUE")) {
            out = true;
        } else if (res.equals("_FALSE")) {
            out = false;
        }
        
        return out;
        
    }
    
}
