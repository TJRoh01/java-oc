/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import TCP.Connection;
import TCP.Connector;
import java.util.concurrent.LinkedBlockingQueue;

/**
 *
 * @author tomas
 */
public class Test {

    public static void main(String argv[]) throws Exception {

        LinkedBlockingQueue<Connection> cons = new LinkedBlockingQueue<>();

        Connector cone = new Connector(6667, cons);

        //while (true) {
        //    System.out.println(cons);
        //}
        while (true) {
            for (Connection con : cons) {

                //con.send("local a = 5; a = a * 3; return a;");
                
                //con.send("{'ayy',67}");
                
                //LuaWrappers.Table tbl = new LuaWrappers.Table(con.recv());
                //System.out.println(tbl.getDouble("ayy"));
                //System.out.println(tbl.getDouble(2));
                //System.out.println(tbl.getString("aa"));
                //System.out.println(tbl.getString(14));
                Lua.Table tbl = new Lua.Table();
                
                tbl.put("ayy", 55.0);
                tbl.put(1, 10);
                tbl.put(2, false);
                tbl.put(3, "true");
                tbl.put(13, 9.0);
                tbl.put(13, 10.0);
                tbl.put("13", "9");
                tbl.put(14, "9");
                tbl.put("aa", "abc");
                tbl.put("ab", false);
                tbl.remove(13);
                StringBuilder dab = new StringBuilder();
                for (int i = 0; i < 10; i++) {
                    dab.append("A");
                    
                }
                
                //System.out.println(tbl.serialize());
                
                //LuaWrappers.Table tbl2 = new LuaWrappers.Table(tbl.serialize());
                //System.out.println(tbl2.serialize());
                //System.out.println(tbl2.getDouble("ayy"));
                //System.out.println(tbl2.getBoolean(2));
                //System.out.println(tbl2.getString(3));
                //System.out.println(tbl.getDouble(13));
                //System.out.println(tbl.getString("13"));
                //System.out.println(tbl.getString(14));
                //System.out.println(tbl.getString("aa"));
                //System.out.println(tbl.getBoolean("ab"));
                
                
                System.out.println("sending");
                tbl.put("dab", dab.toString());

                System.out.println(tbl.serialize());
                con.send(tbl.serialize());
                
                System.out.println("receiving");
                
                String rec = con.recv();
                
                System.out.println(rec);
                
                Lua.Table tbl2 = new Lua.Table(rec);
                
                System.out.println(tbl2.serialize());
                Thread.sleep(10000);

            }
        }
    }

}
