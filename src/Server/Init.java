/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import OC_Ports.Sides;
import OpenComputers.Objects.Generic;
import ROM.RemoteFlasher;
import TCP.Connection;
import TCP.Connection.Msg;
import TCP.Connector;
import Tools.FS;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.LinkedBlockingQueue;

/**
 *
 * @author tomas
 */
public class Init {

    public static void main(String argv[]) throws Exception {     
        // get EEPROM checksum        
        String validChecksumUNIX = FS.read("src/ROM/PXE_ROM_UNIX.CHECKSUM", 8);
        String validChecksumDOS = FS.read("src/ROM/PXE_ROM_DOS.CHECKSUM", 8);

        // setup connector
        LinkedBlockingQueue<Connection> cons = new LinkedBlockingQueue<>();
        Connector connector = new Connector(6666, cons);

        // init connection
        while (true) {

            // establish connection
            Connection con = cons.take();
            System.out.println("Client connected");
            con.send("est", 1000); // inform client connection is established

            // verify checksum
            String checksum = con.srecv(1000);
            System.out.print("PXE_ROM version: " + checksum);       

            if (!checksum.equals(validChecksumUNIX) && !checksum.equals(validChecksumDOS)) {
                System.out.println(" -> OLD!");
                System.out.println("PXE_ROM current: UNIX: " + validChecksumUNIX + ", DOS: " + validChecksumDOS);
                System.out.print("Sending update...");
                con.send((new RemoteFlasher()).toString(), 1000);
                System.out.println(" Sent!");
                
                Msg msg = con.recv(1000);

                if (msg.status.equals("_TRUE")) {
                    System.out.println("Update successful!");
                    System.out.println("Message from client: " + msg.res);
                } else if (msg.status.equals("_FALSE")) {
                    System.out.println("Update failed.");
                    System.out.println("Message from client: " + msg.res);
                }
            } else {
                // if ok then set up environment on client
                System.out.println(" -> OK!");
                System.out.print("Setting up Environment...");
                Generic oco = null;
                try {
                    oco = new Generic(con);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                System.out.println(" Success!");
                
                BufferedReader reader =  new BufferedReader(new InputStreamReader(System.in)); 
                
                Sides sides = new Sides();
                oco.redstone.get(0).setOutput(sides.right(), 0);
                oco.redstone.get(0).setOutput(sides.left(), 0);

                while (true) {
                    System.out.println("> Enter command: spawn x OR kill");
                    String cmd = reader.readLine();
                    if (cmd.toLowerCase().contains("spawn".toLowerCase())) {
                        int amount = Integer.parseInt(cmd.split(" ")[1]);
                        System.out.println("> Spawning " + amount + " sheep");
                        for (int i = 0; i < amount; i++) {
                            oco.redstone.get(0).setOutput(sides.right(), 15);
                            Thread.sleep(50);
                            oco.redstone.get(0).setOutput(sides.right(), 0);
                            Thread.sleep(50);
                        }
                        System.out.println("> DONE");
                    } else if (cmd.toLowerCase().contains("kill".toLowerCase())) {
                        System.out.println("> Killing sheep");
                        oco.redstone.get(0).setOutput(sides.left(), 15);
                        Thread.sleep(100);
                        oco.redstone.get(0).setOutput(sides.left(), 0);
                        Thread.sleep(3000);
                        oco.redstone.get(0).setOutput(sides.left(), 15);
                        Thread.sleep(100);
                        oco.redstone.get(0).setOutput(sides.left(), 0);
                        System.out.println("> DONE");
                    }
                }
        
            }

            System.out.println("");

        }
    }

}
