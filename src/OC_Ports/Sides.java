/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OC_Ports;

import java.util.HashMap;

/**
 *
 * @author tomas
 */
public class Sides {
    
    private final HashMap<String, Integer> aliases = new HashMap<>();
    
    public Sides() {
        this.aliases.put("bottom", 0);
        this.aliases.put("down", 0);
        this.aliases.put("negy", 0);
        this.aliases.put("top", 1);
        this.aliases.put("up", 1);
        this.aliases.put("posy", 1);
        this.aliases.put("back", 2);
        this.aliases.put("north", 2);
        this.aliases.put("negz", 2);
        this.aliases.put("front", 3);
        this.aliases.put("south", 3);
        this.aliases.put("posz", 3);
        this.aliases.put("forward", 3);
        this.aliases.put("right", 4);
        this.aliases.put("west", 4);
        this.aliases.put("negx", 4);
        this.aliases.put("left", 5);
        this.aliases.put("east", 5);
        this.aliases.put("posx", 5);
    }
    
    public int by_alias(String alias) {
        return this.aliases.get(alias);
    }
    
    public int bottom() {
        return 0;
    }
    
    public int top() {
        return 1;
    }
    
    public int back() {
        return 2;
    }
    
    public int front() {
        return 3;
    }
    
    public int right() {
        return 4;
    }
    
    public int left() {
        return 5;
    }
    
}
