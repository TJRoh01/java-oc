/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exceptions;

/**
 *
 * @author tomas
 */
public class UncompileableCode extends Exception {
    public UncompileableCode(String msg) {
        super(msg);
    }
}
