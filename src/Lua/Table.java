/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lua;

import Exceptions.DoubleKeyNotSupported;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 *
 * @author tomas
 */
public class Table {

    //NOTE: namespace is not supported, i.e. a = {}; a.a = 5;
    //NOTE: decimal keys are not supported, i.e. a = {}; a[0.2] = 2;
    private HashMap<Integer, Double> id = new HashMap<>(); // Integer - Double
    private HashMap<Integer, Boolean> ib = new HashMap<>(); // Integer - Boolean
    private HashMap<Integer, String> is = new HashMap<>(); // Integer - String
    private HashMap<String, Double> sd = new HashMap<>(); // String - Double
    private HashMap<String, Boolean> sb = new HashMap<>(); // String - Boolean
    private HashMap<String, String> ss = new HashMap<>(); // String - String

    public Table() {
    }
    
    public Table(String serialized) throws DoubleKeyNotSupported {
        unserialize(serialized);
    }
    
    public String toString() {
        return serialize();
    }
    
    public ArrayList<Integer> getIntegerKeys() {
        ArrayList<Integer> keys = new ArrayList<>();
        
        for (Entry<Integer, Double> entry : id.entrySet()) {
            keys.add(entry.getKey());
        }
        
        for (Entry<Integer, Boolean> entry : ib.entrySet()) {
            keys.add(entry.getKey());
        }
        
        for (Entry<Integer, String> entry : is.entrySet()) {
            keys.add(entry.getKey());
        }
        
        return keys;
    }
    
    public ArrayList<String> getStringKeys() {
        ArrayList<String> keys = new ArrayList<>();
        
        for (Entry<String, Double> entry : sd.entrySet()) {
            keys.add(entry.getKey());
        }
        
        for (Entry<String, Boolean> entry : sb.entrySet()) {
            keys.add(entry.getKey());
        }
        
        for (Entry<String, String> entry : ss.entrySet()) {
            keys.add(entry.getKey());
        }
        
        return keys;
    }
    
    public ArrayList<Double> getDoubleValues() {
        ArrayList<Double> values = new ArrayList<>();

        for (Entry<Integer, Double> entry : id.entrySet()) {
            values.add(entry.getValue());
        }

        for (Entry<String, Double> entry : sd.entrySet()) {
            values.add(entry.getValue());
        }

        return values;
    }
    
    public ArrayList<Boolean> getBooleanValues() {
        ArrayList<Boolean> values = new ArrayList<>();

        for (Entry<Integer, Boolean> entry : ib.entrySet()) {
            values.add(entry.getValue());
        }

        for (Entry<String, Boolean> entry : sb.entrySet()) {
            values.add(entry.getValue());
        }

        return values;
    }
    
    public ArrayList<String> getStringValues() {
        ArrayList<String> values = new ArrayList<>();

        for (Entry<Integer, String> entry : is.entrySet()) {
            values.add(entry.getValue());
        }

        for (Entry<String, String> entry : ss.entrySet()) {
            values.add(entry.getValue());
        }

        return values;
    }

    public double getDouble(int key) {
        return id.get(key);
    }

    public boolean getBoolean(int key) {
        return ib.get(key);
    }

    public String getString(int key) {
        return is.get(key);
    }

    public double getDouble(String key) {
        return sd.get(key);
    }

    public boolean getBoolean(String key) {
        return sb.get(key);
    }

    public String getString(String key) {
        return ss.get(key);
    }

    // returns true if there was an element that was removed
    public boolean remove(int key) {
        boolean removed = false;
        
        if (id.get(key) != null) {
            id.remove(key);
            removed = true;
        }
        if (ib.get(key) != null) {
            ib.remove(key);
            removed = true;
        }
        if (is.get(key) != null) {
            is.remove(key);
            removed = true;
        }
        
        return removed;
    }

    // returns true if there was an element that was removed
    public boolean remove(String key) {
        boolean removed = false;
        
        if (sd.get(key) != null) {
            sd.remove(key);
            removed = true;
        }
        if (sb.get(key) != null) {
            sb.remove(key);
            removed = true;
        }
        if (ss.get(key) != null) {
            ss.remove(key);
            removed = true;
        }
        
        return removed;
    }

    public void put(int key, double val) {
        remove(key);
        id.put(key, val);
    }

    public void put(int key, boolean val) {
        remove(key);
        ib.put(key, val);
    }

    public void put(int key, String val) {
        remove(key);
        is.put(key, val);
    }

    public void put(String key, double val) {
        remove(key);
        sd.put(key, val);
    }

    public void put(String key, boolean val) {
        remove(key);
        sb.put(key, val);
    }

    public void put(String key, String val) {
        remove(key);
        ss.put(key, val);
    }

    private boolean isInteger(String val) {
        try {
            Integer.parseInt(val);
            return true;
        } catch (NumberFormatException ex) {
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    private boolean isDouble(String val) {
        try {
            Double.parseDouble(val);
            return true;
        } catch (NumberFormatException ex) {
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    // delete first and last character by match
    private String delCap(String val, char cap1, char cap2) {
        if (val.charAt(0) == cap1) {
            if (val.charAt(val.length() - 1) == cap2) {
                return val.substring(1, val.length() - 1);
            }
        }
        return val;
    }

    // delete first and last character if they are the same
    private String delCap(String val, char cap) {
        if (val.charAt(0) == cap) {
            if (val.charAt(val.length() - 1) == cap) {
                return val.substring(1, val.length() - 1);
            }
        }
        return val;
    }

    public void unserialize(String serialized) throws DoubleKeyNotSupported {
        // remove first and last character ({, }) and split table elements
        String[] entries = delCap(serialized, '{', '}').split(",");

        int i = 1;

        for (String entry : entries) {
            if (entry.indexOf('=') >= 0) {
                // map part
                String[] keyval = entry.split("=");
                parseIn(keyval[0], keyval[1]);
            } else {
                // array part
                parseIn(Integer.toString(i), entry);
                i++;
            }
        }
    }

    private void parseIn(String key, String val) throws DoubleKeyNotSupported {

        if (key.indexOf('.') >= 0) {
            throw new DoubleKeyNotSupported("");
        }

        // remove brackets if present
        key = delCap(key, '[', ']');

        // store in correct HashMap
        if (isInteger(key) && isDouble(val)) {
            id.put(Integer.parseInt(key), Double.parseDouble(val));
        } else if (isInteger(key) && !isDouble(val)) {
            if (val.equals("true")) {
                ib.put(Integer.parseInt(key), true);
            } else if (val.equals("false")) {
                ib.put(Integer.parseInt(key), false);
            } else {
                is.put(Integer.parseInt(key), delCap(val, '"'));
            }
        } else if (!isInteger(key) && isDouble(val)) {
            sd.put(delCap(key, '"'), Double.parseDouble(val));
        } else if (!isInteger(key) && !isDouble(val)) {
            if (val.equals("true")) {
                sb.put(delCap(key, '"'), true);
            } else if (val.equals("false")) {
                sb.put(delCap(key, '"'), false);
            } else {
                ss.put(delCap(key, '"'), delCap(val, '"'));
            }
        }

    }

    public String serialize() {
        
        String serialized = "{";
        
        // list (ArrayList) part of table
        int i = 1;
        boolean list = true;
        
        // copy HashMaps to not be destructive
        HashMap<Integer, Double> idc = new HashMap<>(id);
        HashMap<Integer, Boolean> ibc = new HashMap<>(ib);
        HashMap<Integer, String> isc = new HashMap<>(is);
        
        while (list) {
            if (idc.get(i) != null) {
               serialized += idc.get(i) + ",";
               idc.remove(i);
            } else if (ibc.get(i) != null) {
               serialized += ibc.get(i) + ",";
               ibc.remove(i);
            } else if (isc.get(i) != null) {
               serialized += "\"" + isc.get(i) + "\",";
               isc.remove(i);
            } else {
                list = false;
            }
            
            i++;
        }
        
        // dictionary (HashMap) part of table
        for (Entry<Integer, Double> entry : idc.entrySet()) {
            serialized += "[" + entry.getKey() + "]=" + entry.getValue() + ",";
        }

        for (Entry<Integer, Boolean> entry : ibc.entrySet()) {
            serialized += "[" + entry.getKey() + "]=" + entry.getValue() + ",";
        }

        for (Entry<Integer, String> entry : isc.entrySet()) {
            serialized += "[" + entry.getKey() + "]=\"" + entry.getValue() + "\",";
        }

        for (Entry<String, Double> entry : sd.entrySet()) {
            serialized += "[\"" + entry.getKey() + "\"]=" + entry.getValue() + ",";
        }

        for (Entry<String, Boolean> entry : sb.entrySet()) {
            serialized += "[\"" + entry.getKey() + "\"]=" + entry.getValue() + ",";
        }

        for (Entry<String, String> entry : ss.entrySet()) {
            serialized += "[\"" + entry.getKey() + "\"]=\"" + entry.getValue() + "\",";
        }

        // remove last "," and add "}"
        return serialized.substring(0, serialized.length() - 1) + "}";
    }

}
