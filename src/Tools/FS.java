package Tools;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author tomas
 */
public class FS {
    
    public static String read(String path) throws IOException {
        return read(path, Integer.MAX_VALUE/8);
    }

    public static String read(String path, int expected_size) throws FileNotFoundException, IOException {
        char[] data = new char[expected_size];
        int size = (new FileReader(path)).read(data, 0, expected_size);
        return String.valueOf(data).substring(0, size);
    }

}
