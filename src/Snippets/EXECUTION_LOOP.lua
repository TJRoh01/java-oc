while true do
    
    local f = load(_RX(), "=EXECUTION_LOOP")
    
    if not (f) then
        _TX("_UNCOM")
        _TX("=EXECUTION_LOOP")
    else
        local status, res = pcall(f)
        
        if not (res) then
            res = "_NONE"
        end
        
        if (status) then
            _TX("_TRUE")
            _TX(res)
        else
            _TX("_FALSE")
            _TX(res)
        end
    end
end
