/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TCP;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.concurrent.LinkedBlockingQueue;

/**
 *
 * @author tomas
 */
public class Connector extends Thread {

    private final ServerSocket socket;
    private LinkedBlockingQueue<Connection> cons;
    public final String ip;
    public final int port;

    public Connector(int port, LinkedBlockingQueue<Connection> cons) throws IOException {
        this.socket = new ServerSocket(port);
        this.cons = cons;
        this.ip = InetAddress.getLocalHost().toString().split("/")[0];
        this.port = port;
        start();
    }
    
    public void close() throws IOException {
        socket.close();
        for (Connection con : cons) {
            con.close();
        }
    }

    @Override
    public void run() {
        while (!socket.isClosed()) {
            try {
                cons.add(new Connection(socket.accept()));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
    
}
