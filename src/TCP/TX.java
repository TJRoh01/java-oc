/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TCP;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.LinkedBlockingQueue;

/**
 *
 * @author tomas
 */
public class TX extends Thread {

    private LinkedBlockingQueue<String> txq;
    private final DataOutputStream out;
    private final Socket socket;

    TX(Socket socket, LinkedBlockingQueue<String> txq) throws IOException {
        this.socket = socket;
        this.txq = txq;
        this.out = new DataOutputStream(socket.getOutputStream());
        start();
    }

    public void run() {
        while (!socket.isClosed()) {
            try {
                out.writeBytes(txq.take());
            } catch (SocketException ex) {
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }

}
