/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TCP;

import Exceptions.ClientError;
import Exceptions.NoReadyStatus;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author tomas
 */
public class Connection {

    private final Socket socket;
    private final LinkedBlockingQueue<String> rxq;
    private final LinkedBlockingQueue<String> txq;
    private final RX rx;
    private final TX tx;

    Connection(Socket socket) throws IOException {
        this.socket = socket;
        this.rxq = new LinkedBlockingQueue<>();
        this.txq = new LinkedBlockingQueue<>();
        this.rx = new RX(socket, rxq);
        this.tx = new TX(socket, txq);
    }
    
    public class Msg {
        
        public final String status;
        public final String res; 

        public Msg(String status, String res) {
            this.status = status;
            this.res = res;
        }
        
        public String toString() {
            return status + " : " + res;
        }
    }

    // send    
    public void send(String msg, int timeout) throws InterruptedException, NoReadyStatus, ClientError {
        txq.add(msg);

        // set the timeout so that the client has time to receive the entire message
        String ready = rxq.poll(timeout, TimeUnit.MILLISECONDS);
        if (ready == null) {
            throw new ClientError("");
        }
        if (!ready.equals("_READY")) {
            throw new NoReadyStatus(ready);
        }
    }
    
    // full receive
    public Msg recv(int timeout) throws InterruptedException, ClientError {
        String status = rxq.poll(timeout, TimeUnit.MILLISECONDS);
        String res = rxq.poll(timeout, TimeUnit.MILLISECONDS);
        
        if (status == null) {
            throw new ClientError("status");
        }
        
        if (res == null) {
            throw new ClientError("res");
        }
        
        return new Msg(status, res);
    }
    
    // single receive    
    public String srecv(int timeout) throws InterruptedException, ClientError {        
        String rec = rxq.poll(timeout, TimeUnit.MILLISECONDS);
        
        if (rec == null) {
            throw new ClientError("");
        }
        
        return rec;
    }

    void close() throws IOException {
        socket.close();
    }

}
