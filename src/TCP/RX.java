/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TCP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.LinkedBlockingQueue;

/**
 *
 * @author tomas
 */
public class RX extends Thread {

    private LinkedBlockingQueue<String> rxq;
    private final BufferedReader in;
    private final Socket socket;

    public RX(Socket socket, LinkedBlockingQueue<String> rxq) throws IOException {
        this.socket = socket;
        this.rxq = rxq;
        this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        start();
    }

    @Override
    public void run() {
        while (!socket.isClosed()) {
            int size = 0;
            char[] msg = new char[1048576]; // can receive 1 MiB max

            try {
                // get message size
                size = in.read(msg, 0, 1048576);
                // only add to received queue if message is not empty
                if (size > 0) {
                    rxq.add(String.valueOf(msg).substring(0, size));
                }
            } catch (StringIndexOutOfBoundsException | SocketException ex) {
                // socket was closed by client / socket is closed
            }
             catch (IOException ex) {
                ex.printStackTrace();
            }

            // client closed socket
            if (size == -1) {
                try {
                    socket.close();
                } catch (SocketException ex) {
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

        }

    }

}
